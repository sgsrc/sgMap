#include <cmath>
#include <cstdio>
#include <ctime>
#include <string>
#include <ogr_api.h>
#include "sg_plugin.h"

// ********** typecasting function ************ 
OGRApplicationH s_hApp = 0;
OGRFeatureCanvasH s_hFtCvs = 0;
OGRFeatureLayerH s_hFtLayer = 0;
OGRCanvasH s_hCvs = 0;
std::string s_strLibName;

static void OGRGetParameter(OGRParametersH hParams)
{
	if(hParams == 0) return;
	OGRParameterH hParam = 0;
	hParam = OGR_PTS_GetParameter(hParams, "Application");
	if(hParam != 0) s_hApp = OGR_PT_GetHandle(hParam);
	hParam = OGR_PTS_GetParameter(hParams, "FeatureCanvas");
	if(hParam != 0) s_hFtCvs = OGR_PT_GetHandle(hParam);
	hParam = OGR_PTS_GetParameter(hParams, "FeatureLayer");
	if(hParam != 0) s_hFtLayer = OGR_PT_GetHandle(hParam);
	hParam = OGR_PTS_GetParameter(hParams, "Canvas");
	if(hParam != 0) s_hCvs = OGR_PT_GetHandle(hParam);
	hParam = OGR_PTS_GetParameter(hParams, "Library");
	if(hParam != 0) s_strLibName = OGR_PT_GetString(hParam);
}

// ********** extern function ************ 
#if defined(PLUGINEXTERN_EXPORT)
#define PLUGINEXTERN extern "C" __declspec(dllexport)
#else 
#define PLUGINEXTERN extern "C" __declspec(dllimport)
#endif
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define FUNC_INIT	qt_init
#define FUNC_DO		qt_do

PLUGINEXTERN const char* GetName()		{ return "qt_plugin"; }
PLUGINEXTERN const char* GetDescription(){ return "qt library"; }
PLUGINEXTERN const char* GetVendor()		{ return "dounggiduk@gmail.com"; }
PLUGINEXTERN const char* GetDocURL()		{ return "http://sgmap.sgsrc.net/"; }
PLUGINEXTERN const char* GetInitName()		{ return TOSTRING(FUNC_INIT); }

std::string s_strOperName[] = {TOSTRING(FUNC_DO)};
std::string s_strOperDescription[] = {"\n"};
std::string s_strOperVersion[] = {"1.0"};

PLUGINEXTERN int GetNumOperations()		{ return sizeof(s_strOperName)/sizeof(std::string); }
PLUGINEXTERN const char* GetOperName(int i)		{ return s_strOperName[i].c_str(); }
PLUGINEXTERN const char* GetOperDescription(int i) { return s_strOperDescription[i].c_str(); }
PLUGINEXTERN const char* GetOperSourceVersion(int i)	{ return s_strOperVersion[i].c_str(); }

// ********** real function ************ 
PLUGINEXTERN void FUNC_INIT(void* param);
PLUGINEXTERN bool FUNC_DO(void* param);
// **********************************

// QT based Event
#include <QtCore\qtranslator.h>
#include <QtGui\qapplication.h>
#include <QtGui\qmainwindow.h>
#include <QtGui\qmenubar.h>
#include <QtGui\qmenu.h>
#include <QtGui\qmessagebox.h>
#include <QtCore\qfileinfo.h>

#include "sg_qt.h"

SGPlugin s_oPluginEvent;
QMainWindow* s_MainWidget;
QString GetLoadLanguagePack()
{
	QString strLang = OGR_UT_GetConfigOption("MSG_LAN");
	QFileInfo info(QString::fromUtf8(s_strLibName.c_str()));
	return (info.path() + "/../config/languages/" + info.baseName() + "_" + (strLang != "" ? strLang : "en_US"));
}

QTranslator s_trsLang;
void FUNC_INIT(void* param)
{
	// init mapper gui
	OGRParametersH hParams = (OGRParametersH) param;
	OGRParameterH hErrParam = 0;
	if(hParams != 0) hErrParam = OGR_PTS_GetParameter(hParams, "error");
	OGRGetParameter(hParams);

	QString name = GetLoadLanguagePack();
	s_trsLang.load(name);
	qApp->installTranslator(&s_trsLang);

	// add menu
	s_MainWidget = (QMainWindow*) OGR_APP_GetMainWindow(s_hApp);
	QString strMenuRoot = "mnuExtPlugIn";
	QString strMenuTitle = QApplication::tr("&PlugIn");
	QMenu* poMenu = (QMenu*)OGR_APP_GetMenu(s_hApp, strMenuRoot.toUtf8().constData(), strMenuTitle.toUtf8().constData());
	QAction* poAction = poMenu->addAction(QApplication::tr("demo"));
	poAction->setObjectName("mAtnPlugIn");
	// connect event
	QObject::connect(poAction, SIGNAL( triggered(bool) ), &s_oPluginEvent, SLOT(on_mAtnPlugIn_triggered(bool)) );
}

void SGPlugin::on_mAtnPlugIn_triggered(bool)
{
	SGPlugin_MenubaseDialog dlg;
	dlg.exec();
}