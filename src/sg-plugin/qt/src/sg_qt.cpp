#include <cmath>
#include <cstdio>
#include <ctime>
#include <map>
#include <string>
#include <ogr_api.h>
#include "sg_plugin.h"

extern OGRApplicationH s_hApp;
extern OGRFeatureCanvasH s_hFtCvs;
extern OGRFeatureLayerH s_hFtLayer;
extern OGRCanvasH s_hCvs;

bool qt_do(void* param)
{
	OGRParametersH hParams = (OGRParametersH) param;
	OGRProgressFunc pfnProgress = 0;
	void* pfnProgressData = 0;
	OGRParameterH hErrParam = 0;
	if(hParams != 0) hErrParam = OGR_PTS_GetParameter(hParams, "error");

	return 0;
}

#include <QtGui\qapplication.h>
#include <QtGui\qmainwindow.h>
#include <QtGui\qmenubar.h>
#include <QtGui\qmenu.h>
#include "sg_qt.h"

SGPlugin_MenubaseDialog::SGPlugin_MenubaseDialog(QWidget* parent) : QDialog(parent)
{
	setupUi(this);
}

SGPlugin_MenubaseDialog::~SGPlugin_MenubaseDialog()
{
}