#ifndef _SG_PLUGIN_H_INCLUDED
#define _SG_PLUGIN_H_INCLUDED

#include <map>
#include <QtCore\qobject.h>

typedef int (*OGRProgressFunc)(double dfComplete, const char *pszMessage, void *pProgressArg);
typedef void *OGRProgressFuncH;

class SGPlugin : public QObject
{
    Q_OBJECT

protected Q_SLOTS:
    void on_mAtnPlugIn_triggered(bool);
};

#endif // _SG_PLUGIN_H_INCLUDED